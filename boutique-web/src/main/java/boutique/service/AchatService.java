/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package boutique.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.ArrayList;
import tg.univlome.cic.boutique.entites.Achat;
import tg.univlome.cic.boutique.entites.Employe;
import java.util.Objects;
import tg.univlome.cic.boutique.entites.ProduitAchete;

/**
 *
 * @author eugene
 */
public class AchatService {

    private static AchatService instance;
    private static List<Achat> liste;

    private EmployeService employeService;
    private ClientService clientService;

    public AchatService() {
        liste = new ArrayList<>();
        employeService = EmployeService.getinstance();
        clientService = ClientService.getinstance();

        liste.add(new Achat(1l, LocalDateTime.of(2020, 1, 1, 11, 50, 0), 500, employeService.trouver(1l), clientService.trouver(1l), new ArrayList<ProduitAchete>()));
        liste.add(new Achat(2l, LocalDateTime.of(2020, 5, 27, 16, 5, 40), 1500, employeService.trouver(2l), clientService.trouver(3l), new ArrayList<ProduitAchete>()));
    }

    public static synchronized AchatService getinstance() {
        if (instance == null) {
            instance = new AchatService();
        }

        return instance;
    }

    public List<Achat> lister() {
        return liste;
    }

    public Achat trouver(Long id) {
        for (Achat p : liste) {
            if (Objects.equals(p.getId(), id)) {
                return p;
            }
        }
        return null;
    }

    public int compter() {
        return liste.size();
    }

    public void ajouter(Achat p) {
        if (p != null) {
            liste.add(p);
        }
    }

    public void modifier(Achat p) {
        if (liste.contains(p)) {
            liste.set(liste.indexOf(p), p);
        }
    }

    public void supprimer(Long id) {
        liste.remove(id.intValue());
    }
}
