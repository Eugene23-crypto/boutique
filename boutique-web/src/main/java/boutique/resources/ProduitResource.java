/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package boutique.resources;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import tg.univlome.cic.boutique.entites.Produit;
import boutique.service.ProduitService;
import java.util.List;
import javax.ws.rs.PathParam;

/**
 *
 * @author eugene
 */
@Path("produit")
public class ProduitResource {

    private ProduitService service;

    public ProduitResource() {
        this.service = ProduitService.getInstance();
    }

    @GET
    public List<Produit> lister() {
        return this.service.lister();
    }

    @GET
    @Path("{id}")
    public Produit trouver(@PathParam("id") Long id) {
        return this.service.trouver(id);
    }

    @GET
    @Path("total")
    public int compter() {
        return this.service.compter();
    }

    @POST
    public void ajouter(Produit p) {
        this.service.ajouter(p);
    }

    @PUT
    public void modifier(Produit p) {
        this.service.modifier(p);
    }

    @DELETE
    @Path("{id}")
    public void supprimer(@PathParam("id") Long id) {
        this.service.supprimer(id);
    }
}
