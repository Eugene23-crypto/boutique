package tg.univlome.cic.boutique.web;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Configures Jakarta RESTful Web Services for the application.
 * @author eugene
 */
@ApplicationPath("api")
public class BoutiqueRestConfiguration extends Application {
    
}
