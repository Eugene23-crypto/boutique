/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tg.univlome.cic.boutique.client;

import java.util.List;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import tg.univlome.cic.boutique.entites.Achat;

/**
 *
 * @author eugene
 */
public class AchatClient {
    private static AchatClient instance;
    private final String URL = "http://localhost:8080/boutique-web/api/produit";
    private Client client;

    public AchatClient() {
        this.client = ClientBuilder.newClient();
    }

    public static synchronized AchatClient getInstance() {
        if (instance == null) {
            instance = new AchatClient();
        }

        return instance;
    }
   public List<Achat> lister() {
       Response response = this.client.target(URL)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();

        if (response.getStatus() == 200) {
            String produits = response.readEntity(String.class);
            System.out.println(produits);
        } else {
            System.out.println(response.getStatus());
            
        }
        return null;
   }
    public Achat trouver(Long id) {
        Response response = this.client.target(URL)
                .path("/" + id)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();

        if (response.getStatus() == 200) {
            Achat achat = response.readEntity(Achat.class);
            return achat;
        } else {
            System.out.println(response.getStatus());
            return null;
        }
    }

    public int compter() {
        Response response = this.client.target(URL)
                .path("/total")
                .request()
                .get();

        if (response.getStatus() == 200) {
            int total = response.readEntity(Integer.class);
            return total;
        } else {
            System.out.println(response.getStatus());
            return -1;
        }
    }
//
   public void ajouter(Achat e) {
       Response response = this.client.target(URL)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.json(e));

        if (response.getStatus() == 204) {
            System.out.println("ajouté");
        } else {
            System.out.println(response.getStatus());
        }
    }
//
    public void modifier(Achat e) {
        Response response = this.client.target(URL)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .put(Entity.json(e));

        if (response.getStatus() == 204) {
            System.out.println("modifié");
        } else {
            System.out.println(response.getStatus());
        }
    }

  public void supprimer(Long id) {
      Response response = this.client.target(URL)
                .path("/" + id)
                .request()
                .delete();

        if (response.getStatus() == 204) {
            System.out.println("supprimé");
        } else {
            System.out.println(response.getStatus());
        }
   }
}
