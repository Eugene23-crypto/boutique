/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tg.univlome.cic.boutique.client;
import java.util.List;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import tg.univlome.cic.boutique.entites.Produit;
import tg.univlome.cic.boutique.entites.ProduitAchete;

/**
 *
 * @author eugene
 */
public class ProduitAcheteClient {
    private static ProduitAcheteClient instance;
    private final String URL = "http://localhost:8080/boutique-web/api/produit";
    private final Client client;

    public ProduitAcheteClient() {
        this.client = ClientBuilder.newClient();
    }

    public static synchronized ProduitAcheteClient getInstance() {
        if (instance == null) {
            instance = new ProduitAcheteClient();
        }

        return instance;
    }
   public List<ProduitAchete> lister() {
       Response response = this.client.target(URL)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();

        if (response.getStatus() == 200) {
            String produits = response.readEntity(String.class);
            System.out.println(produits);
        } else {
            System.out.println(response.getStatus());
            
        }
        return null;
   }
    public Produit trouver(Long id) {
        Response response = this.client.target(URL)
                .path("/" + id)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();

        if (response.getStatus() == 200) {
            Produit produit = response.readEntity(Produit.class);
            return produit;
        } else {
            System.out.println(response.getStatus());
            return null;
        }
    }

    public int compter() {
        Response response = this.client.target(URL)
                .path("/total")
                .request()
                .get();

        if (response.getStatus() == 200) {
            int total = response.readEntity(Integer.class);
            return total;
        } else {
            System.out.println(response.getStatus());
            return -1;
        }
    }
}
