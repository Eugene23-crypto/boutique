/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package tg.univlome.cic.boutique.client;

import java.time.LocalDate;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;
import tg.univlome.cic.boutique.entites.Categorie;
import tg.univlome.cic.boutique.entites.Produit;

/**
 *
 * @author eugene
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String url = "http://localhost:8080/boutique-web/api/produit";
        //String url = "http://192.168.43.230:8080/boutique/api/produit";
//        Client client = ClientBuilder.newClient();
//        Response response = client.target(url)
//                             .path("/1")
//                             .request()
//                             .get(); 
//
//        if (response.getStatus() == 200) {
//            Produit produit = response.readEntity(Produit.class);
//            System.out.println(produit);
//        } else {
//            System.out.println(response.getStatus());
//        }
        ProduitClient produitClient= ProduitClient.getInstance();
        System.out.println(produitClient.trouver(1l));
        //System.out.println(produitClient.compter());
        //System.out.println(produitClient.lister());
        //produitClient.supprimer(1l);
//        Produit nouveauProduit = new Produit(1l, "Fils", 1000.0, LocalDate.of(2025, 12, 1), new Categorie(1l, "Chaussure", "Mocassin, Paire, ..."));
        //produitClient.ajouter(nouveauProduit);
//        produitClient.modifier(nouveauProduit);
    }
    
}
